<?php
namespace Rubeus\Query\ObjQuery;

class Group{
    private $clasula=array();
    
    public function __construct($valor=false) {
        if($valor)$this->add($valor);
    }
    
    public function add($valor){
        $this->clasula[] = $valor;
        return $this;
    }
          
    public function string(){
        foreach ($this->clasula as $c){
            if(is_string($c))
                $valor[] = $c;
            else $valor[] = $c->string();
        }
        return ' group by '.implode(',',$valor).' ';
    }
}