<?php
namespace Rubeus\Query\ObjQuery;

class Limit{
    private $clasula=array();
    
    public function __construct($valor=false,$valorPadrao=0) {
        if($valor)$this->add($valor,$valorPadrao);
    }
    
    public function add($valor,$valorPadrao=0){
        if(!$valorPadrao)$valorPadrao=0;
        $this->clasula = array('valor' => $valor, 'padrao' => $valorPadrao);
        return $this;
    }
          
    public function string(){
        if(empty($this->clasula)) return '';
        if(!is_object($this->clasula['valor']))
            $string  = $this->clasula['padrao'].', '.$this->clasula['valor'];
        else $string = $this->clasula['padrao'].', '.$this->clasula['valor']->string();
        return ' limit '.$string.' ';
    }
}