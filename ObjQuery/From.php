<?php
namespace Rubeus\Query\ObjQuery;

class From{
    private $clasula=array();
    
    public function __construct($tabela=false,$as=false) {
        if($tabela)$this->add($tabela,$as);
    }
    
    public function add($tabela,$as=false){
        $this->clasula[] = array('tabela' => $tabela,'as' => $as);
        return $this;
    }
   
    public function string(){
        $valores = array();
        foreach ($this->clasula as $c){
            $string  = '';
            if($c['as']) $string = ' as '.$c['as'];
            if(is_string($c['tabela']))
                $valores[] = $c['tabela'].$string;
            else $valores[] = $c['tabela']->string().$string;
        }
        return ' from '.implode(', ', $valores);
    }
}