<?php
namespace Rubeus\Query\ObjQuery;

class Order{
    private $clasula=array();
    
    public function __construct($valor=false,$valorPadrao='asc') {
        if($valor)$this->add($valor,$valorPadrao);
    }
    
    public function add($valor,$valorPadrao='asc'){
        $this->clasula[] = array('valor' => $valor, 'padrao' => $valorPadrao);
        return $this;
    }
          
    public function string(){
        if(empty($this->clasula)) return '';
        foreach ($this->clasula as $c){
            if(is_string($c['valor']))
                $valor[] = $c['valor'].' '.$c['padrao'];
            else $valor[] = $c['valor']->string().' '.$c['padrao'];
        }
        return ' order by '.implode(',',$valor).' ';
    }
}