<?php
namespace Rubeus\Query\ObjQuery;

class Union{
    private $clasula=array();
    
    public function __construct($query, $tipo='') {
        if($query)$this->add($query,$tipo);
    }
    
    public function add($query,$tipo=''){
        $this->clasula[] = array('query' => $query, 'tipo' => $tipo);
        return $this;
    }
          
    public function string(){
        $qtd = count($this->clasula);
        $string  = ' ';
        for($i = 0; $i < $qtd; $i++){
            $string .= ') union ('.$this->clasula[$i]['tipo'];
           
            if(is_string($this->clasula[$i]['query']))
                $string .= ' '.$this->clasula[$i]['query'];
            else $string .= ' ('.$this->clasula[$i]['query']->string().') ';
        }
        return $string;
    }
    
     public function limpar(){
        $this->clasula = array();
    }
}