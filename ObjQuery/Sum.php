<?php
namespace Rubeus\Query\ObjQuery;

class Sum{
    private $clasula=array();
    
    public function __construct($valor=false) {
        if($valor)$this->add($valor);
    }
    
    public function add($valor){
        $this->clasula = $valor;
        return $this;
    }
          
    public function string(){
        $string = ' sum(';
        if(is_string($this->clasula))
            $string .= $this->clasula;
        else $string .= $this->clasula->string();
        return $string.') ';
    }
}