<?php
namespace Rubeus\Query\ObjQuery;

class Select{
    private $clasula=array();
    
    public function __construct($valor=false,$valor2=false) {
        if($valor)$this->add($valor,$valor2);
    }
    
    public function add($valor,$valor2=false){
        if($valor2)$this->clasula[] = array("valor" => $valor, "as" => $valor2);
        else $this->clasula[] = array("valor" => $valor);
        return $this;
    }
          
    public function string(){
        $valores = array();
        foreach ($this->clasula as $c){
            $string  = '';
            if(isset($c['as'])) $string = ' as '.$c['as'];
            if(is_string($c['valor']))
                $valores[] = $c['valor'].$string;
            else $valores[] = $c['valor']->string().$string;
        }
        return ' select '.implode(', ', $valores);
    }
    
    public function limpar(){
        $this->clasula = array();
    }
}