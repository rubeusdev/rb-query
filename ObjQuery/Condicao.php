<?php
namespace Rubeus\Query\ObjQuery;

class Condicao{
    private $clasula=array();
    private $like;
    
    public function __construct($valor=false,$valor2=false) {
        $this->like = new Like();
        if($valor)$this->add($valor,$valor2);
    }
    
    public function add($condicao,$anterior=false){
        if($anterior)$this->clasula[] = array('valor' => $anterior, 'ligacao' => $condicao);
        else $this->clasula[] = array('valor' => $condicao);
        return $this;
    }
         
    public function like($condicao, $campo,$like=false,$filtro=false){
        if($like === false)$this->add($this->like->string($condicao,$campo));
        else if($filtro === false)$this->add($condicao, $this->like->string($campo, $like));
        else $this->add($condicao, $this->like->string($campo, $like, $filtro));
        return $this;
    }
    
    public function string(){
        $qtd = count($this->clasula);
        $string  = '';
        for($i = 0; $i < $qtd; $i++){
            if(isset($this->clasula[$i]['ligacao']) && $i > 0) 
                $string .= ' '.$this->clasula[$i]['ligacao'];
            else if( $i > 0)$string .= ' and';
            
            if(is_string($this->clasula[$i]['valor']))
                $string .= ' '.$this->clasula[$i]['valor'];
            else $string .= ' ('.$this->clasula[$i]['valor']->string().') ';
        }
        return $string;
    }
}