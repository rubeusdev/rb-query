<?php
namespace Rubeus\Query\ObjQuery;

class Ifnull{
    private $clasula=array();
    
    public function __construct($valor=false,$valorPadrao=0, $compare=false, $operador='=') {
        if($valor){
            $this->add($valor,$valorPadrao,$compare,$operador);
        }
    }
    
    public function add($valor,$valorPadrao=0, $compare=false, $operador='='){
        $this->clasula = array( 'valor' => $valor, 
                                'padrao' => $valorPadrao, 
                                'compare' => $compare, 
                                'operador' => $operador);
        return $this;
    }
          
    public function string(){
        $string = ' ifnull(';
        if(is_string($this->clasula['valor']))
            $string .= $this->clasula['valor'].',';
        else $string .= $this->clasula['valor']->string().',';
        
        if(is_string($this->clasula['padrao']))
            $string .= $this->clasula['padrao'];
        else $string .= $this->clasula['padrao']->string();
        
        if($compare){
            if(is_string($this->clasula['compare']))
                $string .= $this->clasula['operador'].' '.$this->clasula['compare'];
            else $string .= $this->clasula['operador'].' '.$this->clasula['compare']->string();
        }
        
        return $string.') ';
    }
}