<?php
namespace Rubeus\Query\ObjQuery;

class Concat{
    private $clasula=array();
    
    public function __construct($valor=false) {
        if($valor)$this->add($valor);
    }
    
    public function add($valor){
        $this->clasula[] = $valor;
        return $this;
    }
          
    public function string(){
        $string = ' concat(';
        $qtd = count($this->clasula);
        for($i=0;$i<$qtd;$i++){
            if($i>0)$string .= ',';
            if(is_string($this->clasula))
                $string .= $this->clasula;
            else $string .= $this->clasula->string();
        }
        return $string.') ';
    }
}