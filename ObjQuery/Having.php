<?php
namespace Rubeus\Query\ObjQuery;

class Having{
    private $clasula=array();
    
    public function __construct($valor=false,$valor2=false) {
        if($valor)$this->add($valor,$valor2);
    }
    
    public function add($condicao,$anterior=false){
        if($anterior)$this->clasula[] = array('valor' => $anterior, 'ligacao' => $condicao);
        else $this->clasula[] = array('valor' => $condicao);
        return $this;
    }
          
    public function string(){
        $qtd = count($this->clasula);
        $string  = ' having ';
        for($i = 0; $i < $qtd; $i++){
            if(isset($this->clasula[$i]['ligacao']) && $i > 0) 
                $string .= ' '.$this->clasula[$i]['ligacao'];
            
            if(is_string($this->clasula[$i]['valor']))
                $string .= ' '.$this->clasula[$i]['valor'];
            else $string .= ' ('.$this->clasula[$i]['valor']->string().') ';
        }
        return $string;
    }
    
     public function limpar(){
        $this->clasula = array();
    }
}