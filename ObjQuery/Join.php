<?php
namespace Rubeus\Query\ObjQuery;

class Join{
    private $clasula=array();
    private $i;
    
    public function __construct($tabela=false,$as='',$tipo='inner') {
        $this->like = new Like();
        $this->i = 0;
        if($tabela)$this->add($tabela,$as,$tipo);
    }
    
    public function add($tabela,$as,$tipo='inner'){
        $this->i++;
        $this->clasula[$this->i] = array('tabela' => $tabela,'as' => $as, 'tipo' => $tipo);
        return $this;
    }
     
     public function on($condicao,$anterior=false){
        if($anterior)$this->clasula[$this->i]['condicao'][] = array('valor' => $anterior, 'ligacao' => $condicao);
        else $this->clasula[$this->i]['condicao'][] = array('valor' => $condicao);
        return $this;
    }
    
    public function like($condicao, $campo,$like=false,$filtro=false){
        if($like === false)$this->on($this->like->string($condicao,$campo));
        else if($filtro === false)$this->on($condicao, $this->like->string($campo, $like));
        else $this->on($condicao, $this->like->string($campo, $like, $filtro));
        return $this;
    }
    
    public function string(){
        $string = "";
        foreach($this->clasula as $cal){
            if ($cal['tipo'] == 'straight') 
                $join = 'straight_join';
            else
                $join = $cal['tipo'].' join';

            if(is_string($cal['tabela']))
                $string .= ' '.$join.' '.$cal['tabela'];
            else 
                 $string .= ' '.$join.' '.$cal['tabela']->string();

            if(trim($cal['as']) !== ''){
                $string = $string.' as '.$cal['as'];
            }
            if(isset($cal['condicao'])){
                $qtd = count($cal['condicao']);
                if($qtd > 0)$string .= ' on ';
                for($i = 0; $i < $qtd; $i++){
                    if(isset($cal['condicao'][$i]['ligacao']) && $i > 0) 
                        $string .= ' '.$cal['condicao'][$i]['ligacao'];
                    else if($i > 0) $string .= ' and';

                    if(is_string($cal['condicao'][$i]['valor']))
                        $string .= ' '.$cal['condicao'][$i]['valor'];
                    else $string .= ' ('.$cal['condicao'][$i]['valor']->string().') ';
                }
            }
        }
        return $string;
    }
}