<?php
namespace Rubeus\Query\ObjQuery;

class CaseQuery{   
    private $clasula=array();
    private $else=false;
    
    public function __construct($valor=false,$valor2=false) {
        if($valor)$this->add($valor,$valor2);
    }
    
    public function add($valor,$valor2=false){
        if($valor2)$this->clasula[] = array("when" => $valor, "then" => $valor2);
        else $this->else = $valor;
        return $this;
    }
          
    public function string(){
        $string = ' case ';
        foreach ($this->clasula as $c){
            if(is_object($c['when']))
                $string .= ' when '.$c['when']->string();
            else $string .= ' when '.$c['when'];
             
            if(is_object($c['then']))
                $string .= ' then '.$c['then']->string();                
            else $string .= ' then '.$c['then'];
        }
        if(is_string($this->else))$string .= ' else '.$this->else;
        else if($this->else)$string .= ' else '.$this->else->string();
        return $string.' end ';
    }
}