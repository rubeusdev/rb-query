<?php
namespace Rubeus\Query\ObjQuery;

class With{
    private $clasula=array();
    
    public function __construct($valor=false,$valor2=false) {
        if($valor)$this->add($valor,$valor2);
    }
    
    public function add($valor,$valor2){
        if($valor2)$this->clasula[] = array("as" => $valor, "table" => $valor2);
        return $this;
    }
          
    public function string(){
        $valores = array();
        foreach ($this->clasula as $c){
            if(is_string($c['table']))
                $valores[] = $c['as'].' as ('.$c['table'].')';
            else $valores[] =  $c['as'].' as ('.$c['table']->string().')';
        }
        return 'with '.implode(', ', $valores);
    }
    
    public function limpar(){
        $this->clasula = array();
    }
}