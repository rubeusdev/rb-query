<?php
namespace Rubeus\Query\ObjQuery;

class Like{
    
    public function string($campo,$like, $comparacao='prt'){
        
        if(is_object($campo)) $string = $campo->string();
        else $string = $campo;
        switch ($comparacao){
            case 'ig':
                return " ".$string." like '".$like."'";
            case 'prt':
                return " ".$string." like '%".addslashes($like)."%'";
            case 'cmc':
                return " ".$string." like '%".addslashes($like)."'";
            case 'fim':
                return " ".$string." like '".addslashes($like)."%'";
            case 'sm_ac':
                return "convert(cast(".$string." as binary) Using utf8) like '%".utf8_decode($like)."%'";
        }
    }
          
    
}