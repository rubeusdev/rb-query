<?php

namespace Rubeus\Query;

use Rubeus\Bd\Persistencia as Persistencia;

class Query
{
    private $query;
    private $arrayQuery;
    private $variaveis;
    private $ordem = array('with', 'select', 'from', 'join', 'lateral', 'where', 'group', 'having', 'order', 'limit', 'union');
    private $estruturar;

    public function __construct()
    {
        $this->inicializar();
        $this->estruturar = new Estruturar();
    }

    private function inicializar()
    {
        $this->query = array();
        $this->arrayQuery = array();
        $this->variaveis = array();
    }

    public function addVariaveis($valor)
    {
        if (count($this->variaveis) > 0 && is_array($valor)) {
            $this->variaveis = array_merge($this->variaveis, $valor);
        } else if (count($this->variaveis) > 0 || !is_array($valor))  $this->variaveis[] = $valor;
        else $this->variaveis = $valor;
        return $this;
    }

    public function getVariaveis()
    {
        return $this->variaveis;
    }

    public function limparIndice($indice)
    {
        $this->query[$indice]->limpar();
        return $this;
    }

    public function with($as = false, $valor = false)
    {
        if (!$valor && !$as && isset($this->query['with'])) return $this->query['with'];
        if (isset($this->query['with'])) return $this->query['with']->add($as, $valor);
        return $this->query['with'] = new ObjQuery\With($as, $valor);
    }

    public function select($valor = false, $as = false)
    {
        if (!$valor && !$as && isset($this->query['select'])) return $this->query['select'];
        if (isset($this->query['select'])) return $this->query['select']->add($valor, $as);
        return $this->query['select'] = new ObjQuery\Select($valor, $as);
    }

    public function from($tabela = false, $as = false)
    {
        if (!$tabela && !$as && isset($this->query['from'])) return $this->query['from'];
        if (isset($this->query['from'])) return $this->query['from']->add($tabela, $as);
        return $this->query['from'] = new ObjQuery\From($tabela, $as);
    }

    public function join($tabela = false, $as = '', $tipo = 'inner')
    {
        if (!$tabela && isset($this->query['join'])) return $this->query['join'];
        if (isset($this->query['join'])) return $this->query['join']->add($tabela, $as, $tipo);
        return $this->query['join'] = new ObjQuery\Join($tabela, $as, $tipo);
    }

    public function lateral($tabela = false, $as = '', $tipo = 'inner')
    {
        if (!$tabela && isset($this->query['lateral'])) return $this->query['lateral'];
        if (isset($this->query['lateral'])) return $this->query['lateral']->add($tabela, $as, $tipo);
        return $this->query['lateral'] = new ObjQuery\Lateral($tabela, $as, $tipo);
    }

    public function where($condicao = false, $anterior = false)
    {
        if (!$condicao && !$anterior && isset($this->query['where'])) return $this->query['where'];
        if (isset($this->query['where'])) return $this->query['where']->add($condicao, $anterior);
        return $this->query['where'] = new ObjQuery\Where($condicao, $anterior);
    }

    public function having($condicao = false, $anterior = false)
    {
        if (!$condicao && !$anterior && isset($this->query['having'])) return $this->query['having'];
        if (isset($this->query['having'])) return $this->query['having']->add($condicao, $anterior);
        return $this->query['having'] = new ObjQuery\Having($condicao, $anterior);
    }

    public function order($atributo = false, $order = false)
    {
        if (!$atributo && !$order && isset($this->query['order'])) return $this->query['order'];
        if (isset($this->query['order'])) return $this->query['order']->add($atributo, $order);
        return $this->query['order'] = new ObjQuery\Order($atributo, $order);
    }

    public function union($query = false, $tipo = '')
    {
        if (!$query && !$tipo && isset($this->query['union'])) return $this->query['union'];
        if (isset($this->query['union'])) return $this->query['union']->add($query, $tipo);
        return $this->query['union'] = new ObjQuery\Union($query, $tipo);
    }

    public function group($atributo = false)
    {
        if (!$atributo && isset($this->query['group'])) return $this->query['group'];
        if (isset($this->query['group'])) return $this->query['group']->add($atributo);
        return $this->query['group'] = new ObjQuery\Group($atributo);
    }

    public function limit($quantidade = false, $inicio = false)
    {
        if (!$quantidade && isset($this->query['limit'])) return $this->query['limit'];
        return $this->query['limit'] = new ObjQuery\Limit($quantidade, $inicio);
    }

    public function ifnull($valor = false, $valor2 = false, $compare = false, $operador = '=')
    {
        return new ObjQuery\Ifnull($valor, $valor2, $compare, $operador);
    }

    public function addCase($nome = false, $valor = false, $valor2 = false)
    {
        if ($valor && $valor2 === false)
            return new ObjQuery\CaseQuery($nome, $valor);
        if ($nome) {
            if (!isset($this->arrayQuery[$nome]))
                $this->arrayQuery[$nome] = new ObjQuery\CaseQuery($valor, $valor2);
            else $this->arrayQuery[$nome]->add($valor, $valor2);
            return $this->arrayQuery[$nome];
        }
        return new ObjQuery\CaseQuery($valor, $valor2);
    }

    public function sum($valor = false)
    {
        return new ObjQuery\Sum($valor);
    }

    public function concat($valor = false)
    {
        return new ObjQuery\Concat($valor);
    }

    public function condicao($ligacao = false, $valor = false, $nome = false)
    {
        if ($nome) {
            if (!isset($this->arrayQuery[$nome]))
                $this->arrayQuery[$nome] = new ObjQuery\Condicao($ligacao, $valor);
            else  $this->arrayQuery[$nome]->add($ligacao, $valor);
            return $this->arrayQuery[$nome];
        }
        return new ObjQuery\Condicao($ligacao, $valor);
    }

    public function string()
    {
        $string = '';
        foreach ($this->ordem as $value)
            if (isset($this->query[$value])) $string .= $this->query[$value]->string();
        return "($string)";
    }

    //filtro AA A {indice} AA1 EA
    /**
     * @param string $filtro `A` - Retorna apenas a primeira linha.  
     *                      `AA` (Padrão) - Retorna um array de arrays associativos.  
     *                      `AA1` - Retorna um array com apenas os dados de uma coluna passada em `$indice`.  
     *                      `AE` - Retorna a estrutura personalizada passada em `$indice`.  
     *                      `{coluna}` - Retorna apenas o valor referenciado na coluna.  
     * @param mixed $valorPadrao Valor a ser retornado caso a consulta não tenha um resultado.
     * @param string|array $indice Valor da coluna ou estrutura a ser retornados caso utilize o filtro `AA1` ou `AE`.
     * @param integer $limite Limita o retorno da consulta.
     * @param boolean $getPonteiro Retorna a consulta montada (?)
     * 
     * @return mixed Resultado da consulta.
     */
    public function executar($filtro = 'AA', $valorPadrao = false, $indice = false, $limite = false, $getPonteiro = false)
    {
        $retorno = Persistencia::consultar($this->variaveis, $this->string(), $limite, $getPonteiro);
        if ($getPonteiro) {
            return $retorno;
        }
        if ($limite) {
            $this->filtrar($filtro, $retorno['dados'], $indice);
            if ($retorno['dados'] === false) $retorno = $valorPadrao;
        } else {
            $this->filtrar($filtro, $retorno, $indice);
            if ($retorno === false) $retorno = $valorPadrao;
        }
        return $retorno;
    }

    public function filtrar($filtro, &$retorno, $indice = false)
    {
        switch ($filtro) {
            case 'A':
                if (isset($retorno[0]) && !is_null($retorno[0]) && !is_null($retorno))
                    $retorno = $retorno[0];
                else if (!isset($retorno[0]) || is_null($retorno[0]) || is_null($retorno))
                    $retorno = false;
                break;
            case 'AA':
                if (!isset($retorno[0]) || is_null($retorno[0]) || is_null($retorno))
                    $retorno = false;
                break;
            case 'AA1':
                if (!isset($retorno[0]) || is_null($retorno[0]) || is_null($retorno))
                    $retorno = false;
                else {
                    $dados = array();
                    foreach ($retorno as $r)
                        $dados[] = $r[$indice];
                    $retorno = $dados;
                }
                break;
            case 'EA':
                if (!isset($retorno[0]) || is_null($retorno[0]) || is_null($retorno)) {
                    $retorno = false;
                } else {
                    $retorno = $this->estruturar->estrutura($retorno, $indice);
                }
                break;
            default:
                $indice = str_replace(array('{', '}'), '', $filtro);
                if (isset($retorno[0][$indice]) && !is_null($retorno[0][$indice]))
                    $retorno = $retorno[0][$indice];
                else $retorno = false;
        }
    }

    public function get($indice)
    {
        return $this->query[$indice];
    }

    public function limpar()
    {
        $this->inicializar();
        return $this;
    }
}
