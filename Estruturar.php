<?php
namespace Rubeus\Query;

class Estruturar{    
      
     private function dadosNivel(&$estrutura,&$dados,&$dadosNivel, $nivel=0,$ligacao=''){
        foreach($estrutura as $chave=>$valor){
            if(!is_array($valor) && isset($dados[$chave])){
                $dadosNivel[$nivel.$ligacao][] = ['valor' => $dados[$chave],'chave'=>$chave, 'as' => $valor ];  
            }else if(is_array($valor)){
                $dadosNivel['ligacao'][$nivel.$ligacao][] = $chave; 
                $this->dadosNivel($valor, $dados, $dadosNivel, $nivel + 1,$chave);
            }
        }
    } 
      
    private function inserirEstrutura(&$dadoEstruturado,&$dadosNivel, $nivel=0,$ligacao=''){
        $indiceEncontrado = $existe = 0;
        $qtd = count($dadoEstruturado);
        for($i=0;$i<$qtd;$i++){
            if($this->compativel($dadoEstruturado[$i], $dadosNivel[$nivel.$ligacao])){
                $existe = 1;
                $indiceEncontrado = $i;
                break;              
            }                
        }   
        if($existe == 0 && isset($dadosNivel[$nivel.$ligacao])){
            $indiceEncontrado = $indice = count($dadoEstruturado);
            $resultado = $this->inserir($dadosNivel[$nivel.$ligacao]);
            if(!empty($resultado)){
                $dadoEstruturado[$indice] = $resultado;
                if(isset($dadosNivel['ligacao'][$nivel.$ligacao])){
                    $qtd2 = count($dadosNivel['ligacao'][$nivel.$ligacao]);
                    for($i=0;$i<$qtd2;$i++){
                        $dadoEstruturado[$indice][$dadosNivel['ligacao'][$nivel.$ligacao][$i]] = array();
                    }
                }
            }
        }
        if(isset($dadosNivel['ligacao'][$nivel.$ligacao])){   
            $qtd3 =  count($dadosNivel['ligacao'][$nivel.$ligacao]);
            for($i=0;$i< $qtd3;$i++){
                if (is_array($dadoEstruturado[$indiceEncontrado][$dadosNivel['ligacao'][$nivel.$ligacao][$i]])) {
                    $chave = $dadosNivel['ligacao'][$nivel.$ligacao][$i];
                    $this->inserirEstrutura($dadoEstruturado[$indiceEncontrado][$dadosNivel['ligacao'][$nivel.$ligacao][$i]], $dadosNivel, $nivel + 1,$chave);
                }
            }
        }
    } 
    
    private function inserir($dadosNivel){
        $resultado = array();
        if (is_array($dadosNivel)) {
            $qtd = count($dadosNivel);
            for($j=0;$j<$qtd;$j++){
                if(!is_null($dadosNivel[$j]['valor'])){
                    $resultado[$dadosNivel[$j]['as']] = $dadosNivel[$j]['valor']; 
                }
            }
        }
        return $resultado;
    }
    
    private function compativel($resultado,$dadosNivel){
        for($j=0;$j<count($dadosNivel);$j++){
            if(!((!isset($resultado[$dadosNivel[$j]['as']]) && is_null($dadosNivel[$j]['valor'])) || 
                    (isset($resultado[$dadosNivel[$j]['as']]) && $resultado[$dadosNivel[$j]['as']] == $dadosNivel[$j]['valor']))){
                return false;
            }
        }
        return true;
    }
    
    public function estrutura($dados,$estrutura){
        $dadoEstruturado=array();
        $qtd = count($dados);
        for($i=0;$i<$qtd;$i++){
            $dadosNivel = array();
            $this->dadosNivel($estrutura, $dados[$i], $dadosNivel);
            $this->inserirEstrutura($dadoEstruturado,$dadosNivel);
        }
        return $dadoEstruturado;
    }    
}
